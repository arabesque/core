import * as tape from "tape";
import {createApplication, Middleware} from "../../src";
import {createListenerMock} from "../mocks/listener.mock";
import {spy} from "sinon";

tape('Arabesque', ({test}) => {
    test('should call the listener', ({same, end}) => {
        const listener = createListenerMock();
        const listenerSpy = spy(listener);

        const start = createApplication<string, string>(listenerSpy, () => Promise.resolve('bar'));

        return start('foo').then((stop) => {
            return stop();
        }).finally(() => {
            same(listenerSpy.callCount, 1, 'the listener start function was called once');

            end();
        });
    });

    test('should fulfill the listener with the resolved value of the middleware', ({same, end}) => {
        const onFulfilled = spy(() => {
            return () => Promise.resolve();
        });

        const listener = createListenerMock(onFulfilled);
        const listenerSpy = spy(listener);

        const start = createApplication<string, string>(listenerSpy, () => Promise.resolve('bar'));

        return start('foo').then((stop) => {
            return stop();
        }).finally(() => {
            same(onFulfilled.firstCall.firstArg, 'bar');

            end();
        });
    });

    test('should fulfill the listener with the value passed as parameter to the next callback', ({same, end}) => {
        const onFulfilled = spy(() => {
            return () => Promise.resolve();
        });

        const listener = createListenerMock(onFulfilled);
        const listenerSpy = spy(listener);

        const start = createApplication<string, string>(listenerSpy, (_context, next) => next('bar'));

        return start('foo').then((stop) => {
            return stop();
        }).finally(() => {
            same(onFulfilled.firstCall.firstArg, 'bar');

            end();
        });
    });

    test('should execute middleware', ({same, end}) => {
        const middleware = spy<Middleware<string>>((context, next) => {
            return next(context);
        });

        const listener = createListenerMock();
        const start = createApplication<string, string>(listener, middleware);

        return start('foo').then((stop) => {
            return stop();
        }).finally(() => {
            same(middleware.callCount, 1, 'middleware was executed');

            end();
        })
    });
});