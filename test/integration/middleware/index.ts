import * as tape from "tape";
import {createApplication, Listener, Middleware} from "../../../src";
import {spy} from "sinon";
import {createListenerMock} from "../../mocks/listener.mock";

const createLooseMiddleware = (...middlewares: Array<Middleware<any>>): Middleware<any> => {
    return (context, next) => {
        const invokeMiddlewares = (context: any, middlewares: Array<Middleware<any>>): Promise<any> => {
            const middleware = middlewares[0];

            if (!middleware) {
                return Promise.resolve(context);
            }

            return middleware(context, (context) => {
                return invokeMiddlewares(context, middlewares.slice(1));
            });
        };

        return invokeMiddlewares(context, middlewares).then(next);
    };
};

/**
 * *** ************************************************************** ***
 * *** What is breaking the chain? Not executing the next middleware. ***
 * *** ************************************************************** ***
 */
tape('Middleware implementation', ({test}) => {
    const passThrough = spy((context, next) => {
        return next(context);
    });

    const breakTheChainWithFoo = spy(() => {
        return Promise.resolve('foo');
    });

    const returnBar = spy((_context, next) => {
        return next('bar');
    });

    test('A "loose" middleware that allows its children to break the chain', ({test}) => {
        test('should ...', ({end, same}) => {
            const middleware = createLooseMiddleware(passThrough, breakTheChainWithFoo, returnBar);

            const onFulfilled = spy(() => {
                return () => Promise.resolve();
            });

            const listener: Listener<any, any> = (channel, handle) => {
                return handle(channel).then(onFulfilled);
            };

            const start = createApplication(listener, middleware);

            return start('channel').then((stop) => {
                return stop();
            }).finally(() => {
                same(passThrough.callCount, 1);
                same(breakTheChainWithFoo.callCount, 1);
                same(returnBar.callCount, 0);
                same(onFulfilled.firstCall.firstArg, 'foo');

                passThrough.resetHistory();
                breakTheChainWithFoo.resetHistory();
                returnBar.resetHistory();

                end();
            });
        });
    });

    test('A "strict" middleware that prevents its children to break the chain', ({end, same}) => {
        const createMiddleware = (...middlewares: Array<Middleware<any>>): Middleware<any> => {
            return (context, next) => {
                const invokeMiddlewares = (context: any, middlewares: Array<Middleware<any>>): Promise<any> => {
                    const middleware = middlewares[0];

                    if (!middleware) {
                        return Promise.resolve(context);
                    }

                    return middleware(context, (context) => {
                        return Promise.resolve(context);
                    }).then((context) => {
                        return invokeMiddlewares(context, middlewares.slice(1));
                    });
                };

                return invokeMiddlewares(context, middlewares).then(next);
            };
        };

        const middleware = createMiddleware(passThrough, breakTheChainWithFoo, returnBar);

        const onFulfilled = spy(() => {
            return () => Promise.resolve();
        });

        const listener: Listener<any, any> = (channel, handle) => {
            return handle(channel).then(onFulfilled);
        };

        const start = createApplication(listener, middleware);

        return start('channel').then((stop) => {
            return stop();
        }).finally(() => {
            same(passThrough.callCount, 1);
            same(breakTheChainWithFoo.callCount, 1);
            same(returnBar.callCount, 1);
            same(onFulfilled.firstCall.firstArg, 'bar');

            passThrough.resetHistory();
            breakTheChainWithFoo.resetHistory();
            returnBar.resetHistory();

            end();
        });
    });

    /**
     * todo: reword
     */
    test('A "apply" middleware that acts as its children', ({test}) => {
        const createMiddleware = (...middlewares: Array<Middleware<any>>): Middleware<any> => {
            return (context, next) => {
                const invokeMiddlewares = (context: any, middlewares: Array<Middleware<any>>): Promise<any> => {
                    const middleware = middlewares[0];

                    if (!middleware) {
                        return Promise.resolve(context);
                    }

                    /**
                     * the next callback of the last child is the next callback of the parent
                     */
                    const nextCallback = middlewares.length === 1 ? next : (context: any) => {
                        return invokeMiddlewares(context, middlewares.slice(1));
                    };

                    return middleware(context, nextCallback);
                };

                return invokeMiddlewares(context, middlewares);
            };
        };

        test('should break the chain if one of its children breaks the chain', ({end, same}) => {
            /**
             * Pass-through, then break the chain
             */
            const middleware = createMiddleware(passThrough, breakTheChainWithFoo);

            const nextMiddleware = spy(() => {
                return Promise.resolve()
            });

            const looseMiddleware = createLooseMiddleware(middleware, nextMiddleware);

            const start = createApplication(createListenerMock(), looseMiddleware);

            return start('channel').then((stop) => {
                return stop();
            }).finally(() => {
                same(nextMiddleware.callCount, 0);

                end();
            });
        });

        test('should follow the chain if all of its children follows the chain', ({end, same}) => {
            /**
             * Pass-through, then bar
             */
            const middleware = createMiddleware(passThrough, returnBar);

            const nextMiddleware = spy(() => {
                return Promise.resolve()
            });

            const looseMiddleware = createLooseMiddleware(middleware, nextMiddleware);

            const start = createApplication(createListenerMock(), looseMiddleware);

            return start('channel').then((stop) => {
                return stop();
            }).finally(() => {
                same(nextMiddleware.callCount, 1);

                end();
            });
        });
    });
});