import {Listener} from "../../src";

export const createListenerMock = (onFulfilled = () => {
    return () => Promise.resolve();
}): Listener<any, any> => {
    return (channel, handle) => {
        return handle(channel).then(onFulfilled);
    };
}