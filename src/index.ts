/*************
 * Arabesque *
 *************/
export type Middleware<Context> = (context: Context, next: (context: Context) => Promise<Context>) => Promise<Context>;

export type Listener<Channel, Context> = (channel: Channel, handler: (context: Context) => Promise<Context>) => Promise<() => Promise<void>>;

export type Application<Channel> = (channel: Channel) => Promise<() => Promise<void>>;

export const createApplication = <Channel, Context>(
    listen: Listener<Channel, Context>,
    middleware: Middleware<Context>
): Application<Channel> => {
    return (channel) => {
        return listen(channel, (context) => {
            return middleware(context, (context) => Promise.resolve(context));
        });
    };
};