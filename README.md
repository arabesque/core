# Arabesque

[![NPM version][npm-image]][npm-url] [![Coverage Status][coverage-image]][coverage-url]

> Functional micro-framework

Arabesque is a lightweight, modular and functional micro-framework.

## Getting started

Arabesque provides a minimal foundation to create applications and micro-services, consisting of three types and a function.

### Listener

```ts
declare type Listener<Channel, Context> = (channel: Channel, handler: (context: Context) => Promise<Context>) => Promise<() => Promise<void>>;
```

The fundamental part of an Arabesque application, the Listener is passed by the application a _channel_, that the
Listener is expected to subscribe to, and a _handler_, that the Listener must call whenever it receives something from
the _channel_. The Listener must return a promise that resolves to an argument-less function that, when executed, stop
the Listener.

A _channel_ can take any imaginable form, for example:

* a number in the context of a TCP service, representing a port
* a structured object in the context of a Kafka consumer, representing a broker, a group ID and a topic
* a string in the context of a file watcher, representing a pattern of files to watch

### Middleware

```ts
declare type Middleware<Context> = (context: Context, next: (context: Context) => Promise<Context>) => Promise<Context>;
```

The operational part of an Arabesque application, the Middleware is passed by the application a _context_, coming from
either the Listener or a previous Middleware, and a _next_ function, that the Middleware may call with a Context to
execute the next Middleware, if any.

### Application

```ts
declare type Application<Channel> = (channel: Channel) => Promise<() => Promise<void>>;
```

The Arabesque Application itself that, when executed, returns a Promise that resolves to an argument-less function that,
when executed, stop the Application.

### createApplication

```ts
import {Middleware} from "./index";

declare const createApplication: <Channel, Context>(
    listen: Listener<Channel, Context>,
    middleware: Middleware<Context>
) => Application<Channel, Context>;
```

The factory that creates an Arabesque Application from a Listener and a Middleware.

## Code sample

```ts
import type {Listener} from "@arabesque/core";
import {createApplication} from "@arabesque/core";

const listener: Listener<string, string> = (channel, handler) => {
    return handler(channel).then(() => {
        return () => Promise.resolve();
    });
};

const start = createApplication(listener, (context, next) => {
    console.log(`I am the middleware and I received ${context}`);

    return next(context);
});

return start('foo').then((stop) => {
    return stop();
});
```

## Contributing

* Fork this repository
* Code
* Implement tests using [tape](https://github.com/substack/tape)
* Issue a merge request keeping in mind that all pull requests must reference an issue in the issue queue

[npm-image]: https://badge.fury.io/js/@arabesque%2Fcore.svg

[npm-url]: https://www.npmjs.com/package/@arabesque/core

[coverage-image]: https://coveralls.io/repos/gitlab/arabesque/core/badge.svg

[coverage-url]: https://coveralls.io/gitlab/arabesque/core